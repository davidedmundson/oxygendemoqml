
import QtQuick 2.1
import QtQuick.Controls 1.0 as QtControls
import QtQuick.Layouts 1.0

QtControls.ApplicationWindow
{
    width: 640
    height: 495

    QtControls.GroupBox {
        flat: true
        title: "Show the appearance of text input widgets"
        anchors.fill: parent

        ColumnLayout {
            anchors.fill: parent
            GridLayout {
                columns: 2
                Text {
                    text: "Single line text editor:"
                }
                QtControls.TextField {
                    text: "Example Text"
                    Layout.fillWidth: true
                }
                Text {
                    text: "Password editor:"
                }
                QtControls.TextField {
                    text: "Password"
                    echoMode: TextInput.Password
                    Layout.fillWidth: true
                }
                Text {
                    text: "Editable combobox:"
                }
                Text {
                    text: "FIXME"
                }
                Text {
                    text: "Spinbox:"

                }
                QtControls.SpinBox {
                    Layout.fillWidth: true
                }
            }
            QtControls.CheckBox {
                text: "Use flat widgets"
            }
            //FIXME Horizontal Rule here
            Text {
                text: "Multi-line text editor"
            }
            QtControls.TextArea {
                text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\nSed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"

                Layout.fillWidth: true
                Layout.fillHeight: true
            }

        }

    }

}
