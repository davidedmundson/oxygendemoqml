import QtQuick 2.1
import QtQuick.Controls 1.0 as QtControls
import QtQuick.Layouts 1.0

QtControls.ApplicationWindow
{
    width: 640
    height: 495

    QtControls.GroupBox {
        flat: true
        title: "Show the appearance of buttons"
        anchors.fill: parent

        ColumnLayout {
            anchors.fill: parent
            QtControls.GroupBox {
                title: "Pushbuttons"
                Layout.fillWidth: true
            }
            QtControls.GroupBox {
                title: "Toolbuttons"
                Layout.fillWidth: true
            }
            RowLayout {
                QtControls.GroupBox {
                    title: "Tickboxes"
                    Layout.fillWidth: true
                }
                QtControls.GroupBox {
                    title: "Radiobuttons"
                    Layout.fillWidth: true
                }
            }
        }
    }
}
